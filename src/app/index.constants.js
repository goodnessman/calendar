/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('calendar')
    .constant('malarkey', malarkey)
    .constant('moment', moment)

    .constant('config', {
      api: {
        tasks: 'data/tasks.json'
      }
    });

})();
