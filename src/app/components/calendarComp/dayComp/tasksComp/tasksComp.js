(function() {
  'use strict';

  angular
    .module('calendar')
    .component('tasksComp', {
      templateUrl: 'app/components/calendarComp/dayComp/tasksComp/tasksComp.html',
      controller: tasksCompController,
      bindings: {
          tasks: '<',
          day: '<',
          category: '=',
          status: '='
        }
    });

    function tasksCompController(CalendarService) {
      var ctrl = this;

      ctrl.tasks = CalendarService.getTaskToDay(ctrl.day, ctrl.tasks);
    }

})();