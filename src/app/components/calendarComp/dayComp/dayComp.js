(function() {
  'use strict';

  angular
    .module('calendar')
    .component('dayComp', {
      templateUrl: 'app/components/calendarComp/dayComp/dayComp.html',
      controller: dayCompController,
      bindings: {
          tasks: '<',
          td: '<',
          month: '<',
          category: '=',
          status: '='
        }
    });

    function dayCompController($scope, CalendarService) {
      var ctrl = this;

      ctrl.today = CalendarService.checkToday(ctrl.td.day, ctrl.month);
    }

})();