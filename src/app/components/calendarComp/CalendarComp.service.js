(function () {
'use strict';

angular.module('calendar')
.factory('CalendarService', function (moment) {
    var d = new Date(),
        year = d.getFullYear(),
        month = d.getUTCMonth(),
        today = d.getDate(),
        firstDay = new Date(year, month, 1),
        firstWday = firstDay.getDay(),
        oneHour = 1000 * 60 * 60,
        oneDay = oneHour * 24,
        nextMonth = new Date(year, month + 1, 1),
        lastDate = Math.ceil((nextMonth.getTime() - firstDay.getTime() - oneHour) / oneDay),
        months = ["Январь","Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        trigger = d.getUTCMonth();

    var Service = {

        // Get task for day
        getTaskToDay: function (day, tasks) {
            var now = year + '-' + (parseInt(month) + 1) + '-' + day,
                i = 0,
                dayTasks = [];
            
            if (tasks) {
                now = moment(now).format("YYYY-MM-DD");

                for (i; i < tasks.length; i++) {

                    if (tasks[i].date == now) {

                        tasks[i].time = moment(tasks[i].time, "hmm").format("H:mm");

                        dayTasks.push(tasks[i]);
                    }
                }

                if (dayTasks.length) {

                    dayTasks = sortTask(dayTasks);

                    return dayTasks;
                }
            }

            return dayTasks;

            function sortTask(arr) {

                arr.sort(sort);

                return arr;

                function sort(a, b) {

                    a = moment(a.time, 'h:mm:ss');
                    b = moment(b.time, 'h:mm:ss');

                  if (a.isAfter(b)) {

                    return 1;
                  }else {

                    return -1;
                  }
                }
            }
        },

        // Get unical categories
        getCategories: function(tasks) {
            var i = 0,
                catArr = [];

            for (i; i < tasks.length; i++) {

                if (tasks[i].hasOwnProperty('category')) {

                    catArr = checkSimilarCat(catArr, tasks[i].category);
                }
            }

            return catArr;

            function checkSimilarCat(arr, cat) {
                var i = 0;

                for (i; i < arr.length; i++) {

                    if (arr[i] === cat) {

                        return arr;
                    }
                }

                arr.push(cat);

                return arr;
            }
        },

        // Check current day
        checkToday: function(day, month) {

            if (day == today && month == months[d.getUTCMonth()]) {

                return true;
            }

            return false;
        },

        // Get current month
        getMonth: function() {

            return months[month];
        },

        // Formatting days for print
        getDays: function() {
            var days = [],
                i = 0, j = 0;

            firstWday != 0 ? firstWday = firstWday - 1 : firstWday = 0;
            
            for (i; i < lastDate; i++) {

                if ((1 + j) > 31) {
                    
                    break;
                }

                if (i == firstWday) {

                    // days.push(1);
                                  
                    for (j; j < lastDate; j++) {

                        days.push(1 + j);
                    }
                }else {

                    if (j < 10) {

                        days.push(' ');
                    }else {

                        continue;
                    }
                }
            }

            days = formatDays(days);

            return days;


            // Return array with month days
            function formatDays(arr) {
                var days = [],
                    day = {}, 
                    tr = {},
                    counter = 7,
                    i = 0, j = 0;
                
                for (i = 0; i < 6; i++) {

                    tr = {};
                    tr.td = [];

                    for (j; j < counter; j++) {

                        day = {};
                        day.day = arr[j];

                        if (i == 5) {

                            if (!arr[35]) {

                                break;
                            }else {

                                !arr[j] ? tr.td.push(' ') : tr.td.push(day);
                            }
                        }else {

                            !arr[j] ? tr.td.push(' ') : tr.td.push(day);
                        }
                        
                    }

                    counter += 7;
                    days.push(tr);
                }

                return days;
            }
        },

        // Next month
        next: function() {
            if (++trigger === 12) {
                trigger = 0;
            }

            month = trigger;
            today = d.getDate();
            firstDay = new Date(year,month,1);
            firstWday = firstDay.getDay();
            nextMonth = new Date(year, month + 1, 1);
            lastDate = Math.ceil((nextMonth.getTime() - firstDay.getTime() - oneHour)/oneDay);
        },

        // Previos month
        prev: function() {
            if (--trigger === -1) {
                trigger = 11;
            }

            month = trigger;
            today = d.getDate();
            firstDay = new Date(year,month,1);
            firstWday = firstDay.getDay();
            nextMonth = new Date(year, month + 1, 1);
            lastDate = Math.ceil((nextMonth.getTime() - firstDay.getTime() - oneHour)/oneDay);
        }
    };

    return Service;
});

})();