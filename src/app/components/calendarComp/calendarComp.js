(function() {
  'use strict';

  angular
    .module('calendar')
    .component('calendarComp', {
      templateUrl: 'app/components/calendarComp/calendarComp.html',
      controller: calendarCompController
    });

    function calendarCompController($scope, CalendarCompRestService, CalendarService) {
      var ctrl = this;

      ctrl.tasks = [];
      ctrl.categories = [];
      ctrl.month = CalendarService.getMonth();
      ctrl.days = CalendarService.getDays();
      ctrl.catStatus = 'Все';
      ctrl.category = 'Все';
      ctrl.prevMonth = prevMonth;
      ctrl.nextMonth = nextMonth;

      CalendarCompRestService.getTasks()
      .then(function (data) {

          if (data.tasks.length > 0) {

            ctrl.tasks = data.tasks;
            ctrl.categories = CalendarService.getCategories(data.tasks);
          }
      });

      function prevMonth() {

        CalendarService.prev();

        ctrl.month = CalendarService.getMonth();
        ctrl.days = CalendarService.getDays();
      }

      function nextMonth() {

        CalendarService.next();

        ctrl.month = CalendarService.getMonth();
        ctrl.days = CalendarService.getDays();
      }
    }

})();