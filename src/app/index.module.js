(function() {
  'use strict';

  angular
    .module('calendar', ['ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr']);

})();
