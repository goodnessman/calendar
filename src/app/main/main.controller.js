(function() {
  'use strict';

  angular
    .module('calendar')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController() {}
})();
